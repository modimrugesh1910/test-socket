const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http, {origins: '*:*'});
io.set('origins', '*:*');
io.set('transports', ['websocket']);
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/chat.html');
});

io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('joined', function (data) {
        console.log(data);
        socket.emit('acknowledge', 'Acknowledged');
    });
    socket.on('chat message', function (msg) {
        console.log('message: ' + msg);

        const httpGet = (theUrl) =>
        {
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open("GET", theUrl, false); // false for synchronous request
            xmlHttp.send(null);
            return xmlHttp.responseText;
        };

        console.log(httpGet('https://api.mfapi.in/mf/130503'));

        socket.emit('response message', httpGet('https://api.mfapi.in/mf/130503').toString() + '  from server');
        //socket.broadcast.emit('response message', msg + '  from server');
    });
});

http.listen(8080, function () {
    console.log('listening on *:8080');
});